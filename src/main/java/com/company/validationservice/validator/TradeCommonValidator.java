package com.company.validationservice.validator;

import com.company.validationservice.payload.Trade;
import com.company.validationservice.util.TradeValidationUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static com.company.validationservice.util.TradeValidationUtils.isCurrencyWorkingDay;
import static com.company.validationservice.util.TradeValidationUtils.isSupportedCounterparty;
import static com.company.validationservice.util.TradeValidationUtils.isValidCurrencyPair;
import static com.company.validationservice.util.TradeValidationUtils.notEmpty;
import static com.company.validationservice.util.TradeValidationUtils.notNull;

@Component
public class TradeCommonValidator implements Validator {
	private OptionValidator optionValidator;

	public TradeCommonValidator(OptionValidator optionValidator) {
		this.optionValidator = optionValidator;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return Trade.class.isAssignableFrom(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		Trade trade = (Trade) o;
		notEmpty("customer", trade.getCustomer(), errors);
		notEmpty("ccyPair", trade.getCcyPair(), errors);
		notEmpty("legalEntity", trade.getLegalEntity(), errors);
		notEmpty("trader", trade.getTrader(), errors);

		notNull("type", trade.getType(), errors);
		notNull("direction", trade.getDirection(), errors);
		notNull("tradeDate", trade.getTradeDate(), errors);
		notNull("amount1", trade.getAmount1(), errors);
		notNull("amount2", trade.getAmount2(), errors);
		notNull("rate", trade.getRate(), errors);
		notNull("valueDate", trade.getValueDate(), errors);

		if (trade.getValueDate() != null && trade.getTradeDate() != null &&
				trade.getValueDate().isBefore(trade.getTradeDate())) {
			errors.rejectValue("valueDate", "trade.valueDate.isBeforeTradeDate");
		}

		if (!isCurrencyWorkingDay(trade.getValueDate(), trade.getCcyPair())) {
			errors.rejectValue("valueDate", "trade.currency.NotWorkingDay");
		}

		if (!isSupportedCounterparty(trade.getCustomer())) {
			errors.rejectValue("customer", "trade.counterparty.NotSuppoted");
		}
		if (!isValidCurrencyPair(trade.getCcyPair(), errors)) {
			errors.reject("ccyPair", "trade.currency.notValidISO4217");
		}
		if (trade.getType() == Trade.Type.VanillaOption) {
			TradeValidationUtils.invokeValidator(optionValidator, trade, errors);
		}
	}
}
