package com.company.validationservice.validator;

import com.company.validationservice.payload.Trade;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static com.company.validationservice.util.TradeValidationUtils.notNull;

@Component
public class OptionValidator implements Validator {
	private static final String AMERICAN_TYPE = "American";
	private static final String EUROPEAN_TYPE = "European";

	@Override
	public boolean supports(Class<?> aClass) {
		return Trade.class.isAssignableFrom(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		Trade trade = (Trade) o;
		if (!AMERICAN_TYPE.equals(trade.getStyle()) && !EUROPEAN_TYPE.equals(trade.getStyle())) {
			errors.rejectValue("style", "trade.style.notAllowed");
		}

		if (AMERICAN_TYPE.equals(trade.getStyle())) {
			if (trade.getExcerciseStartDate() == null) {
				errors.rejectValue("excerciseStartDate", "trade.excerciseStartDate.empty");
			} else if (trade.getExcerciseStartDate().isBefore(trade.getTradeDate())
					|| trade.getExcerciseStartDate().isAfter(trade.getExpiryDate())) {
				errors.rejectValue("excerciseStartDate", "trade.excerciseStartDate.notValid");
			}
		}
		notNull("expiryDate", trade.getExpiryDate(), errors);
		notNull("deliveryDate", trade.getDeliveryDate(), errors);
		notNull("premiumDate", trade.getPremiumDate(), errors);

		if (trade.getExpiryDate().isAfter(trade.getDeliveryDate())) {
			errors.rejectValue("expiryDate", "trade.expiryDate.notValid");
		}
		if (trade.getPremiumDate().isAfter(trade.getDeliveryDate())) {
			errors.rejectValue("premiumDate", "trade.premiumDate.notValid");
		}
	}
}
