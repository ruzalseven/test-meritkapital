package com.company.validationservice.validator;

import java.util.List;

import com.company.validationservice.payload.Trade;
import com.company.validationservice.payload.TradesBatch;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class TradeBatchValidator implements Validator {
	private TradeCommonValidator commonValidator;

	public TradeBatchValidator(TradeCommonValidator commonValidator) {
		this.commonValidator = commonValidator;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return TradesBatch.class.isAssignableFrom(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		TradesBatch tradesBatch = (TradesBatch) o;
		List<Trade> trades = tradesBatch.getTrades();
		if (trades == null || trades.isEmpty()) {
			errors.rejectValue("trades", "trade.trades.empty");
		} else {
			for (int i = 0; i < trades.size(); i++) {
				errors.pushNestedPath("trades[" + i + "]");
				ValidationUtils.invokeValidator(this.commonValidator, trades.get(i), errors);
				errors.popNestedPath();
			}
		}
	}
}
