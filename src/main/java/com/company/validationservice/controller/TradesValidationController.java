package com.company.validationservice.controller;

import javax.validation.Valid;

import com.company.validationservice.payload.Trade;
import com.company.validationservice.payload.TradesBatch;
import com.company.validationservice.response.ApiResponse;
import com.company.validationservice.response.ValidationResponse;
import com.company.validationservice.service.ValidationService;
import com.company.validationservice.validator.TradeBatchValidator;
import com.company.validationservice.validator.TradeCommonValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TradesValidationController {
	private TradeCommonValidator commonValidator;
	private TradeBatchValidator batchValidator;
	private ValidationService validationService;

	public TradesValidationController(TradeCommonValidator commonValidator,
									  TradeBatchValidator batchValidator,
									  ValidationService validationService) {
		this.commonValidator = commonValidator;
		this.batchValidator = batchValidator;
		this.validationService = validationService;
	}

	@InitBinder("trade")
	protected void initBinderTrade(WebDataBinder binder) {
		binder.setValidator(commonValidator);
	}

	@InitBinder("tradesBatch")
	protected void initBinderTradeBa(WebDataBinder binder) {
		binder.setValidator(batchValidator);
	}

	@PostMapping("/validate")
	public ResponseEntity<? extends ApiResponse> validate(@Valid @RequestBody Trade trade,
														  BindingResult bindingResult) {
		if (bindingResult.getErrorCount() > 0) {
			return handleErrors(bindingResult);
		}
		return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value()));
	}

	private ResponseEntity<ValidationResponse> handleErrors(BindingResult bindingResult) {
		ValidationResponse body = ValidationResponse.builder()
				.withStatus(HttpStatus.BAD_REQUEST.value())
				.withErrors(validationService.resolveTradeErrors(bindingResult))
				.build();
		return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
	}

	@PostMapping("/validateBatch")
	public ResponseEntity validateBatch(@Valid @RequestBody TradesBatch tradesBatch, BindingResult bindingResult) {
		if (bindingResult.getErrorCount() > 0) {
			return handleErrors(bindingResult);
		}
		return ResponseEntity.ok(new ApiResponse(HttpStatus.OK.value()));
	}
}
