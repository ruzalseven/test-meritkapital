package com.company.validationservice.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

@Service
public class ValidationService {
	private final MessageSource messageSource;

	@Autowired
	public ValidationService(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public Map<String, Set<String>> resolveTradeErrors(BindingResult bindingResult) {
		Map<String, Set<String>> errors = new LinkedHashMap<>();
		bindingResult.getFieldErrors().forEach(e ->
				errors.computeIfAbsent(e.getField(), k -> new HashSet<>()).add(messageSource.getMessage(e,
						null)));
		return errors;
	}
}
