package com.company.validationservice.util;

import java.time.LocalDate;
import java.util.Currency;
import java.util.HashSet;
import java.util.Set;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

public class TradeValidationUtils extends org.springframework.validation.ValidationUtils {
	private static final Set<String> ALLOWED_CUSTOMERS;

	static {
		ALLOWED_CUSTOMERS = new HashSet<>();
		ALLOWED_CUSTOMERS.add("PLUTO1");
		ALLOWED_CUSTOMERS.add("PLUTO2");
	}

	public static void notEmpty(String name, String field, Errors errors) {
		if (StringUtils.isEmpty(field)) {
			errors.rejectValue(name, "trade.field.empty");
		}
	}

	public static void notNull(String name, Object o, Errors errors) {
		if (o == null) {
			errors.rejectValue(name, "trade.field.empty");
		}
	}

	public static boolean isCurrencyWorkingDay(LocalDate valueDate, String ccyPair) {
		//todo need working days info
		return true;
	}

	public static boolean isSupportedCounterparty(String customer) {
		return ALLOWED_CUSTOMERS.contains(customer);
	}

	public static boolean isValidCurrencyPair(String ccyPair, Errors errors) {
		if (ccyPair == null || ccyPair.length() < 6) {
			errors.rejectValue("ccyPair", "trade.ccyPair.notValid");
		} else {
			String first = ccyPair.substring(3);
			String second = ccyPair.substring(3, 6);
			try {
				Currency.getInstance(first);
				Currency.getInstance(second);
			} catch (IllegalArgumentException e) {
				return false;
			}
		}
		return true;
	}
}
