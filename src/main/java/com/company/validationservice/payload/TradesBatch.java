package com.company.validationservice.payload;

import java.util.List;

public class TradesBatch {
	private List<Trade> trades;

	public List<Trade> getTrades() {
		return trades;
	}

	public void setTrades(List<Trade> trades) {
		this.trades = trades;
	}
}
