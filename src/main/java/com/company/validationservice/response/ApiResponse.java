package com.company.validationservice.response;

import java.time.LocalDateTime;

public class ApiResponse {
	private LocalDateTime timestamp;
	private int status;

	public ApiResponse() {
	}

	public ApiResponse(int status) {
		this.timestamp = LocalDateTime.now();
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
}
