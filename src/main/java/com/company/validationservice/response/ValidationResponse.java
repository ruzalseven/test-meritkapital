package com.company.validationservice.response;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

public class ValidationResponse extends ApiResponse {
	private Map<String, Set<String>> errors;

	public static ValidationResponseBuilder builder() {
		return new ValidationResponseBuilder();
	}

	public Map<String, Set<String>> getErrors() {
		return errors;
	}

	public void setErrors(Map<String, Set<String>> errors) {
		this.errors = errors;
	}

	public static final class ValidationResponseBuilder {
		private int status;
		private Map<String, Set<String>> errors;

		private ValidationResponseBuilder() {
		}

		public ValidationResponseBuilder withStatus(int status) {
			this.status = status;
			return this;
		}

		public ValidationResponseBuilder withErrors(Map<String, Set<String>> errors) {
			this.errors = errors;
			return this;
		}

		public ValidationResponse build() {
			ValidationResponse validationResponse = new ValidationResponse();
			validationResponse.setTimestamp(LocalDateTime.now());
			validationResponse.setStatus(status);
			validationResponse.setErrors(errors);
			return validationResponse;
		}
	}
}
