package com.company.validationservice;

import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.util.Strings;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
//TODO NEED MORE TIME TO ADD ALL CORRECT TEST CASES
@Ignore
public class ValidationServiceApplicationTests {

	private static final ObjectMapper mapper = new ObjectMapper();
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void validate() throws Exception {
		mockMvc.perform(post("/validate")
				.content(TestData.trades[0]).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().is4xxClientError());
	}

	@Test
	public void validateBatch() throws Exception {
		mockMvc.perform(post("/validateBatch")
				.content("{trades: [ " + Strings.join(Arrays.asList(TestData.trades), ',') + " ]}")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().is2xxSuccessful());
	}

}
